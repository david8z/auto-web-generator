from django.db import models

from django.db.models.signals import pre_save

from django.core.validators import MinValueValidator, MaxValueValidator


from django.utils.text import slugify
from datetime import datetime
from category.models import Category
from image.models import Image


class Article(models.Model):
    title = models.CharField(max_length=100)

    introduction = models.TextField(max_length=5000, blank=True, null=True)
    conclusion = models.TextField(max_length=5000, blank=True, null=True)
    keyword = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    slug = models.SlugField(unique=True, max_length=100, blank=True)


    publication_date = models.DateField(default=datetime.now, blank=True)

    image = models.ForeignKey(Image, on_delete=models.SET_NULL, blank=True, null=True, related_name="article_image")
    volume = models.PositiveIntegerField(db_index=True)
    top_10 = models.BooleanField(default=False)
    shared_social = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    corrected_text = models.BooleanField(default=False)

    stars = models.FloatField(validators=[MaxValueValidator(5.0), MinValueValidator(0.0)], default=0)
    valuations = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'

    def __str__(self):
        return "{}".format(self.title)

def articulo_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    if not instance.slug:
        instance.slug = slugify(instance.title)

pre_save.connect(articulo_pre_save_receiver, sender=Article)
