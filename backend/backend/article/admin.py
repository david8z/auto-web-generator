from django.contrib import admin
from django.utils.html import mark_safe

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields


from article.models import Article
from text.models import Text

class TextInline(admin.TabularInline):
    model = Text


class ArticleResource(resources.ModelResource):
    class Meta:
        model = Article

@admin.register(Article)
class ArticleAdmin(ImportExportModelAdmin):
    resource_class = ArticleResource
    list_display = ('slug', 'volume', 'published', 'thumb', 'num_texts', 'category')
    list_filter = ('published', 'shared_social', 'top_10', 'category')
    ordering = ('-volume',)

    fieldsets = (
        ('MAIN CONTENT', {'fields': ('title', 'introduction', 'keyword', 'category', 'image', 'publication_date', 'stars', 'valuations')}),
        ('CONTENT STATS', {'fields': ('volume', 'corrected_text', 'top_10', 'published', 'shared_social')})
    )
    inlines = [TextInline, ]
    def thumb(self, obj):
        if obj.image:
            return mark_safe('<img src="/media/%s" width="%d" height="90" />' % (obj.image.image, obj.image.ratio * 90))
        else:
            return 'No Image Found'
        
    def num_texts(self, obj):
        return obj.article_text.count()


