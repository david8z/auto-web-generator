from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Prefetch


from text.models import Text
from article.models import Article
from article.serializers import PostsDestacadosSerializer, UltimosPostsSerializer, PostSerializer


class PostsDestacadosViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener los 4 PostsDestacados.
    """
    serializer_class = PostsDestacadosSerializer
    http_method_names = ['get']
    # lookup_field = 'slug'

    def get_queryset(self):
        # Cogemos los tres articulos con menor volumen que no están en el top 10  
        return Article.objects.order_by('volume').select_related('image').select_related('category').filter(top_10=False)[:4]
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = PostsDestacadosSerializer(queryset, many=True)
        return Response(serializer.data)

class UltimosPostsViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener los 4 ÚltimosPosts.
    """
    serializer_class = UltimosPostsSerializer
    http_method_names = ['get']
    # lookup_field = 'slug'

    def get_queryset(self):
        # Cogemos los tres articulos con menor volumen que no están en el top 10  
        return Article.objects.order_by('-publication_date').exclude(id__in=Article.objects.order_by('volume').select_related('image').select_related('category').filter(top_10=False)[:4])[:4]
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = UltimosPostsSerializer(queryset, many=True)
        return Response(serializer.data)

class PostViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener el post dado el slug.
    """
    serializer_class = PostSerializer
    http_method_names = ['get']
    lookup_field = 'slug'

    def get_queryset(self):
        # Cogemos los tres articulos con menor volumen que no están en el top 10  
        return Article.objects.all()

    def retrieve(self, request, slug):
        queryset = Article.objects.select_related('image').select_related('category').filter(slug=slug).prefetch_related(
            Prefetch(
                "article_text",
                queryset=Text.objects.filter(articulo__slug=slug),
            )
        ).first()
        serializer = PostSerializer(queryset, many=False)
        return Response(serializer.data)