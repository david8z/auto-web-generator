"""DOC STRING"""
import django.core.checks

from rest_framework.response import Response

from rest_framework import serializers
from text.models import Text
from category.serializers import SimpleCategorySerializer
from image.serializers import ImageSerializer
from text.serializers import TextSerializer
from article.models import Article

###############################
##POSTS DESTACADOS SERIALIZER##
###############################
@django.core.checks.register('rest_framework.serializers')
class PostsDestacadosSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    image = serializers.SerializerMethodField('get_image')
    category = serializers.SerializerMethodField('get_category')

    def get_image(self, obj):
        serializer = ImageSerializer(obj.image, many=False)
        return serializer.data
    def get_category(self, obj):
        serializer = SimpleCategorySerializer(obj.category, many=False)
        return serializer.data

    class Meta:
        model = Article
        fields = ['image', 'title', 'introduction', 'category', 'stars', 'valuations', 'slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }


############################
##ULTIMOS POSTS SERIALIZER##
############################
@django.core.checks.register('rest_framework.serializers')
class UltimosPostsSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    image = serializers.SerializerMethodField('get_image')
    category = serializers.SerializerMethodField('get_category')

    def get_image(self, obj):
        serializer = ImageSerializer(obj.image, many=False)
        return serializer.data
    def get_category(self, obj):
        serializer = SimpleCategorySerializer(obj.category, many=False)
        return serializer.data

    class Meta:
        model = Article
        fields = ['image', 'title', 'introduction', 'category', 'publication_date', 'slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }



###################
##POST SERIALIZER##
###################
@django.core.checks.register('rest_framework.serializers')
class PostSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    article_text = TextSerializer(many=True)

    image = serializers.SerializerMethodField('get_image')
    category = serializers.SerializerMethodField('get_category')

    def get_image(self, obj):
        serializer = ImageSerializer(obj.image, many=False)
        return serializer.data
    def get_category(self, obj):
        serializer = SimpleCategorySerializer(obj.category, many=False)
        return serializer.data

    class Meta:
        model = Article
        fields = ['image', 'title', 'introduction', 'conclusion', 'category', 'publication_date', 'stars', 'valuations', 'slug', 'article_text']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }