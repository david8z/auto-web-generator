"""DOC STRING"""
import django.core.checks

from rest_framework import serializers

from category.models import Category
from image.serializers import ImageSerializer


##############################
##SIMPLE CATEGORY SERIALIZER##
##############################
@django.core.checks.register('rest_framework.serializers')
class SimpleCategorySerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Category
        fields = ['title', 'slug']


#########################
##CATEGORIES SERIALIZER##
#########################
@django.core.checks.register('rest_framework.serializers')
class CategorySerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    image = serializers.SerializerMethodField('get_image')

    def get_image(self, obj):
        serializer = ImageSerializer(obj.image, many=False)
        return serializer.data
    class Meta:
        model = Category
        fields = ['image', 'title', 'slug']

