from django.db import models
from django.utils.text import slugify
from django.db.models.signals import pre_save

from image.models import Image

class Category(models.Model):
    slug = models.SlugField(unique=True, max_length=100, blank=True)
    title = models.CharField(max_length=100)
    image = models.ForeignKey(Image, on_delete=models.PROTECT)

    def __str__(self):
        return self.slug
    
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

def categoria_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    if not instance.slug:
        instance.slug = slugify(instance.title)

pre_save.connect(categoria_pre_save_receiver, sender=Category)
