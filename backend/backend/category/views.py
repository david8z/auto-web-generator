from rest_framework import viewsets
from rest_framework.response import Response

from category.models import Category
from category.serializers import CategorySerializer
from article.serializers import PostsDestacadosSerializer

from article.models import Article

class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener los 4 PostsDestacados.
    """
    serializer_class = CategorySerializer
    http_method_names = ['get']
    lookup_field = 'slug'

    def get_queryset(self):
        # Cogemos los tres articulos con menor volumen que no están en el top 10  
        return Category.objects.all()
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Article.objects.select_related('image').select_related('category').filter(category__slug=slug)
        serializer = PostsDestacadosSerializer(queryset, many=True)
        return Response(serializer.data)