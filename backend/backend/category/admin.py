from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields


from category.models import Category

class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category

@admin.register(Category)
class CategoryAdmin(ImportExportModelAdmin):
    resource_class = CategoryResource

    fieldsets = (
        ('MAIN CONTENT', {'fields': ('slug', 'title', 'image')}),
    )
