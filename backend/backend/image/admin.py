from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields


from image.models import Image

class ImageResource(resources.ModelResource):
    class Meta:
        model = Image

@admin.register(Image)
class ImageAdmin(ImportExportModelAdmin):
    resource_class = ImageResource
    readonly_fields = ['image_tag']

    fieldsets = (
        ('MAIN CONTENT', {'fields': ('image', 'image_tag', 'alt', 'ratio')}),
    )
