from django.db import models
from django.utils.html import mark_safe
from django.utils.text import slugify

from PIL import Image as pil_image



from django.db.models.signals import pre_save

class Image(models.Model):
    image = models.ImageField(upload_to="images")
    ratio = models.FloatField(null=True, blank=True)
    alt = models.TextField(max_length=5000, blank=True, null=True)

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="/media/%s" width="%d" height="150" />' % (self.image, self.ratio * 150))
        else:
            return 'No Image Found'

    image_tag.short_description = 'Image'

    def __str__(self):
        return slugify(self.alt)

def image_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    width, height = pil_image.open(instance.image).size  
    instance.ratio = round(width/height, 2)

pre_save.connect(image_pre_save_receiver, sender=Image)
