"""DOC STRING"""
import django.core.checks

from rest_framework import serializers
from image.models import Image

####################
##IMAGE SERIALIZER##
####################
@django.core.checks.register('rest_framework.serializers')
class ImageSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Image
        fields = ['image', 'ratio', 'alt']