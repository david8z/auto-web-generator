"""
backend URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from article.views import PostsDestacadosViewSet, UltimosPostsViewSet, PostViewSet
from category.views import CategoryViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r'posts-destacados', PostsDestacadosViewSet, basename="PostsDestacados")
ROUTER.register(r'ultimos-posts', UltimosPostsViewSet, basename="UltimosPosts")
ROUTER.register(r'categories', CategoryViewSet, basename="Categories")
ROUTER.register(r'posts', PostViewSet, basename="Post")


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(ROUTER.urls))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
