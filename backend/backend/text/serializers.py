"""DOC STRING"""
import django.core.checks

from rest_framework import serializers
from text.models import Text
from image.serializers import ImageSerializer

###################
##TEXT SERIALIZER##
###################
@django.core.checks.register('rest_framework.serializers')
class TextSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    image = serializers.SerializerMethodField('get_image', required=False)

    def get_image(self, obj):
        if obj.image:
            serializer = ImageSerializer(obj.image, many=False)
            return serializer.data
        else:
            return None

    class Meta:
        model = Text
        fields = ['title', 'texto', 'image']