from django.db import models
from article.models import Article
from image.models import Image

class Text(models.Model):
    title = models.CharField(max_length=100)
    texto = models.TextField(max_length=5000, default="")
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, blank=True, null=True)
    articulo = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='article_text')
