import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _bf4b079c = () => interopDefault(import('../pages/aviso-legal.vue' /* webpackChunkName: "pages/aviso-legal" */))
const _d38f492e = () => interopDefault(import('../pages/contacto.vue' /* webpackChunkName: "pages/contacto" */))
const _5d3e5152 = () => interopDefault(import('../pages/politica-de-cookies.vue' /* webpackChunkName: "pages/politica-de-cookies" */))
const _03b4e9a8 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _309ad214 = () => interopDefault(import('../pages/_category/index.vue' /* webpackChunkName: "pages/_category/index" */))
const _42e28166 = () => interopDefault(import('../pages/_category/_post/index.vue' /* webpackChunkName: "pages/_category/_post/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/aviso-legal",
    component: _bf4b079c,
    name: "aviso-legal"
  }, {
    path: "/contacto",
    component: _d38f492e,
    name: "contacto"
  }, {
    path: "/politica-de-cookies",
    component: _5d3e5152,
    name: "politica-de-cookies"
  }, {
    path: "/",
    component: _03b4e9a8,
    name: "index"
  }, {
    path: "/:category",
    component: _309ad214,
    name: "category"
  }, {
    path: "/:category/:post",
    component: _42e28166,
    name: "category-post"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
