export const state = () => ({
  home: {},
  categories: []
})

export const mutations = {
  categories (state, newValue) {
    state.categories = newValue.data
  }
}

export const actions = {
  async nuxtServerInit (vuexContext, context) {
    vuexContext.commit('categories', (await this.$axios.get('/api/categories/')))
  }
}

export const getters = {
  home: state => state.home,
  categories: state => state.categories
}
